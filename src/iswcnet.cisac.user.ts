// ==UserScript==
// @name        [ISWC] iswcnet.cisac.org
// @namespace   GuilhermeHideki
// @match       https://iswcnet.cisac.org/search
// @grant       none
// @version     2023.04.29
// @author      GuilhermeHideki
// @description Creates a function to extract work data from the website
// ==/UserScript==

/**
 * Helper to satisfy Typescript generics
 */
const Q = {
  find: (query: string) => document.querySelector<HTMLElement>(query),
  findInput: (query: string) => document.querySelector<HTMLInputElement>(query),
  findAll: (query: string) => document.querySelectorAll<HTMLElement>(query),
}

/**
 * Helper to satisfy Typescript generics
 */
const El = {
  getValue: (query: string) => Number(Q.findInput(query)?.value),
  getText: (query: string) => Q.findInput(query)?.textContent,
  toArray: (query: string) => Array.from(Q.findAll(query)),
}

function getItems() {
  return El.toArray([
    `[class^='Search']`,
    `div`,
    `[class^='Grid_container']`,
    `[class^='Grid_table']`,
    `table`,
    `tbody`,
    `tr[class^='GridRow_container']`,
  ].join(' > '))
}

const details = {
  show: (item: HTMLElement) => {
    document.querySelectorAll<HTMLElement>('[id="View Less"]').forEach((e) => e.click())
    item.querySelector<HTMLElement>('[id="View More"]')?.click()
  },
  viewMore: () => document.querySelector<HTMLElement>('[class^="ViewMore_viewMoreContainer"]')!,
  sections: [
    {
      key: 'artists',
      selector: (item: HTMLElement) => item.querySelector('[class^="ViewMore_gridContainer"]:nth-of-type(2)'),
      mapper: {
        name: 'Creator Name(s):',
        ipi: 'IP Name Number:',
        affiliation: 'Affiliation:',
        role: 'Rolled Up Role:',
      }
    }
  ]
}

function parseItem(item: HTMLElement) {
  const fields = {
    work: {
      iswc: 'Preferred ISWC:',
      title: 'Original Title:',
    },
  }

  const data: Record<string, unknown> = {}

  Object.entries(fields.work).forEach(([field, query])=> {
    const textContent = item.querySelector(`[id="${query}"] span`)?.textContent
    if (typeof textContent === "string") {
      data[field] = textContent
    }
  })

  details.show(item)

  for (const section of details.sections) {
    const items = section.selector(details.viewMore())
    data[section.key] = []
    items?.querySelectorAll('tbody').forEach((rawData) => {
      const entity: Record<string, string> = {}
      for (const [field, query] of Object.entries(section.mapper)) {
        entity[field] = rawData.querySelector(`[id="${query}"] span`)?.textContent!
      }

      (data[section.key] as unknown[]).push(entity)
    })
  }

  console.debug('data:', data)

  return data
}

function parseAllPages() {
  const getPage = () => El.getValue('[class^="Pagination_inputDiv"] input')
  const getLastPage = () => {
    const text = El.getText('[class^="Pagination_inputDiv"]')
    return Number(text?.match(/of (\d+)/)?.[1] || 1)
  }

  const result: unknown[] = []
  do {
    getItems().forEach(work => result.push(parseItem(work)))

    const next = El.toArray('[class^="PaginationButton_pageNumberButton"]')
      .find(b => b.textContent == "Next" && !b.className.includes('Disabled'))

    if (!next) return result
    next.click()
  } while (getPage() <= getLastPage())

  return result
}

var IWSC = window.IWSC = {
  getItems,
  parseItem,
  parseAllPages,
}
